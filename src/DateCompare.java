import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

/**
 * Created by Parisa Nikzad on 7/10/2017.
 */
public class DateCompare {

    private LocalDate oldDate;
    private LocalDate newDate;
    java.time.Period period;


    public String dateInterval(LocalDate oldDate, LocalDate newDate){
        this.oldDate = oldDate;
        this.newDate = newDate;

        if(newDate.isAfter(oldDate)){

            period = java.time.Period.between(oldDate, newDate);

            return getFormatDate();

        }else{
            return String.format("The date order is wrong");
        }

    }

    public int getPeriodDays(){
        return period.getDays();
    }

    public int getPeriodMonths(){
        return period.getMonths();
    }

    public int getPeriodYears(){
        return period.getYears();
    }

    public String getFormatDate(){
        return String.format("It took " + getPeriodYears() + " years, " + getPeriodMonths() +
                " months, and " + getPeriodDays() + " days");
    }


}
