import org.junit.Test;
import java.time.LocalDate;
import java.time.Month;
import static junit.framework.TestCase.assertNotNull;


/**
 * Created by Parisa Nikzad on 7/10/2017.
 */
public class TestAlpha {

    DateCompare dateCompare = new DateCompare();

    @Test
    public void dateIntervalTest(){

        LocalDate oldDate = LocalDate.of(2015, Month.NOVEMBER, 18);
        LocalDate newDate = LocalDate.of(2017, Month.JANUARY, 28);
        java.time.Period period = java.time.Period.between(oldDate, newDate);

        assertNotNull(dateCompare.dateInterval(oldDate, newDate));

        assert(0 < dateCompare.getPeriodDays() && dateCompare.getPeriodDays() < 32);

        assert(0 < dateCompare.getPeriodMonths() && dateCompare.getPeriodMonths()<13);

    }

}
