import java.time.LocalDate;
import java.time.Month;

/**
 * Created by Parisa Nikzad on 7/10/2017.
 */
public class Driver {

    public static void main(String[] args){

        DateCompare dateCompare = new DateCompare();

        LocalDate oldDate = LocalDate.of(2015, Month.NOVEMBER, 18);
        System.out.println("Old Date is: " + oldDate);

        LocalDate newDate = LocalDate.of(2017, Month.FEBRUARY, 28);
        System.out.println("New Date is: " + newDate);


        System.out.println(dateCompare.dateInterval(oldDate, newDate));

    }
}
